FROM php:7.4-fpm

WORKDIR /var/www

RUN apt-get update && apt-get install -y \
    software-properties-common build-essential \
    git zlib1g-dev libicu-dev g++ libxml2-dev libpq-dev

RUN apt-get install -y openssl libssl-dev curl libcurl4-openssl-dev \
    && docker-php-ext-install pdo pdo_mysql bcmath sockets curl

RUN apt-get update && apt-get install -y mcrypt libmcrypt-dev \
    && pecl install mcrypt-1.0.4 \
    && docker-php-ext-enable mcrypt

RUN apt-get update && apt-get install -y memcached libmemcached-dev \
    && mkdir -p /usr/src/php/ext/memcached \
    && curl -L "https://github.com/php-memcached-dev/php-memcached/archive/refs/tags/v3.1.5.tar.gz" | tar -xzv -C /usr/src/php/ext/memcached --strip-components=1 \
    && docker-php-ext-configure memcached \
    && docker-php-ext-install memcached

RUN git clone https://github.com/phpredis/phpredis.git /usr/src/php/ext/redis \
    && docker-php-ext-install redis

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

RUN apt-get autoremove && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*

RUN chown -R www-data:www-data /var/www

USER www-data

EXPOSE 9000

CMD ["php-fpm"]
